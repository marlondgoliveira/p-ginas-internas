// FUNÇÕES ************************************

/*
Função megaValidar 1.0
Por: Carlos Alexandre Cavalchuki Gazola
Em: 15/01/2014
chame o jQuery
coloque na tag form onsubmit="javascript:return megaValidar(this.id);"
caso queira que valide ao passar de um campo para o outro, adicione na tag input onblur="megaValidar(this.id)"
em cada campo que queira validar apenas coloque seu data-valida conforme abaixo 
     (obs. para campo de confirmação são três atributos data)
data-valida="1" validacao de obrigatoriedade
data-valida="2" validacao de email
data-valida="3"  data-confirma="id_do_campo_principal"  data-campo2="nome do campo a ser exibido no alert"
     validacao de campo de confirmação (ex: digite a senha novamente)
atualmente na v1 só são validados e-mail(simples), obrigatoriedade e confirmação de campo

** REVISADO POR MARLON EM 26/07/2015
*/

function megaValidar(formulario){   
	var ret = true;
	var campo_atual = formulario;
	if($("#"+formulario).data("valida")){                                           
		formulario = $("#"+formulario).closest("form").attr("id");                      
	}                                       
	var alerta = '<div class="alert alert-danger msg-validation"><div class="seta-up"></div><div class="seta-up-borda"></div><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><i class="fa fa-exclamation-triangle fs-22"></i> [::msg::]</div>'
	var msg = "Campo obrigat&oacute;rio!"
	var msg_email = "Deve conter um e-mail v&aacute;lido!"
	var msg_campo2 = "O valor deve ser id&ecirc;ntico ao do campo: "
	$('.msg-validation').remove();
	$('form#'+ formulario +' :input').each(function(){
		var $this = $(this);
		var validator = $this.data("valida");
		if(validator){
			if (validator == '1'){
				if ($this.val().length == 0){
					$this.addClass('error');
					$this.parent('label').addClass('text-danger');
					$this.after(alerta.replace('[::msg::]',msg));
					$this.focus();
					ret = false;
					return false;
				}
				else{
					$this.removeClass('error').addClass('success');
					$this.parent('label').removeClass('text-danger');
					$this.siblings('.msg-validation').remove();
					$this.siblings('label').removeClass('text-danger');
				}
			};

			if (validator == '2'){
				var regExpEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				if (!regExpEmail.test($this.val())){
					$this.addClass('error');
					$this.parent('label').addClass('text-danger');
					$this.after(alerta.replace('[::msg::]',msg_email));
					$this.focus();
					ret = false;
					return false;
				}
				else{
					$this.removeClass('error').addClass('success');
					$this.siblings('.msg-validation').remove();
					$this.parent('label').removeClass('text-danger');
					$this.siblings('label').removeClass('text-danger');
				}

			};

			if (validator == '3'){
				if($this.data("confirma")){
					var campo_2 = $this.data("confirma");
					if ($this.val() != $('#'+campo_2).val()){
						$this.addClass('error');
						$this.after(alerta.replace('[::msg::]',msg_campo2 + $this.data("campo-2") ));
						$this.parent('label').addClass('text-danger');
						$this.focus();
						ret = false;
						return false;
					}

					else{
						$this.parent('label').removeClass('text-danger');
						$this.siblings('label').removeClass('text-danger');
					}
				};
			};
		};
		if(this.id == campo_atual){
			ret = false;
			return false;                                               
		};
	});

	return ret;
};




// SCRIPTS Executados ao carregar a página

$(document).ready(function(){

	// script para ativar automaticamente algumas coisas do bootstrap
	$('[data-toggle="tooltip"], .redes-sociais > a').tooltip({trigger: 'hover'});
	$('[data-toggle="popover"]').popover({trigger: 'hover'});


	/*=========================================================
	*  Funcao atribui mascara
	*  Criado por Danilo Batista de Souza
	*  15/03/2014
	*  Depende do jquery.mask.js || Ja esta incluso no starter.js
	*  E so colocar o atributo data-mask="+MascaraDesejada+"
	===========================================================*/
	jQuery(function($){
		$('[data-mask]').each(function(index, el) {
		   var mascaraCampo = $(this).data('mask');
		   $(this).mask(mascaraCampo);
		});

		/**
		* @author Marlon
		* @since 26/07/2015
		*/
		$('input.mask-fone').focusout(function(){
			var phone, element;
			element = $(this);
			element.unmask();
			phone = element.val().replace(/\D/g, '');
			if(phone.length > 10) {
				element.mask("(99) 9 9999-999?9");
			} else {
				element.mask("(99) 9999-9999?9");
			}
		}).trigger('focusout');
	});


	/*==========================================================================
	* Funcao Gera Iframe 
	* Criado por Danilo Batista de Souza
	* 31/01/2014
	* Adicione o atributo data-iframe-src="" na tag q vc queira q o iframe seja gerado
	* Neste atributo devera ter o link do iframe, e pronto!!!
	* Ex: <div data-iframe-src="http://www.gv8.com.br"></div>
	* modificado por carlos, adicionado atributos altura e largura pra iframe e config 
	* padroes pra iframe
	===========================================================================*/
	setTimeout(function(){
		$('[data-iframe-src]').each(function(){
		  var src = $(this).attr('data-iframe-src');
		  var width = $(this).attr('data-iframe-width');
		  var height = $(this).attr('data-iframe-height');
		  $('<iframe allowtransparency="true" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" ></iframe>').attr('src', src).attr('width', width).attr('height', height).appendTo(this);
		});
	}, 600);


	/*==========================================================
	* Configuracoes da pagina 169
	============================================================*/
	$('[data-muda-video]').click(function(event) {
		var mudaVideoIframe = $(this).data('muda-video');

		event.preventDefault();
		$('.video169 iframe').attr('src', ''+mudaVideoIframe+'');
	});

	$('[data-muda-video]').each(function(index, el) {
		var linkDovideo = $(this).data('muda-video');
		
		var recorteDoVIdeo = linkDovideo.replace('http://www.youtube.com/embed/','');
		var recorteDoVIdeo = recorteDoVIdeo.replace('https://www.youtube.com/embed/','');


		var recorteDoVIdeo = recorteDoVIdeo.replace('http://www.youtube.com/watch?v=','');        
		var recorteDoVIdeo = recorteDoVIdeo.replace('https://www.youtube.com/watch?v=','');

		$(this).css('background-image', 'url(http://img.youtube.com/vi/'+recorteDoVIdeo+'/default.jpg)');
	});
	
	$('[data-desc]').click(function(event) {
		var blocoDeDescricao = $(this).data('desc');

		$('.descroes-videos .descricao-video.active').removeClass('active');
		$(blocoDeDescricao).addClass('active');
	});


	/*===================================================================
	* Videos youtube
	* basta colocar a url d video no attr data-video-src e pronto!
	====================================================================*/
	$('[data-youtube-src]').each(function(index, el) {
		var linkDoYoutube = $(this).data('youtube-src').replace('http://www.youtube.com/watch?v=','').replace('https://www.youtube.com/watch?v=','');
		var videoGerado   = '<iframe src="http://www.youtube.com/embed/'+linkDoYoutube+'" frameborder="0"></iframe>';

		$(this).html(videoGerado)
	});


	/*======================================================
	Paginacao
	=======================================================*/
	$('.paginaca span').addClass('active');

	var paginaAtive = $('.paginacao a.active').text();
	var paginaAgora = '<span class="active">'+paginaAtive+'</span>';

	$('.paginacao a.active').after(paginaAgora);
	$('.paginacao a.active').remove();

	
	// Manual parallax
	//$(document.body).scroll(function(event){$('[data-parallax="active"]').each(function(index,el){var x=$(this).data("parallax-scroll");var yPos=((-1 * $('.wrapper').offset().top)/x);console.log($('.wrapper').offset().top);var bgpos='50% -'+yPos+'px';$(this).css('background-position',bgpos);});});
	// $('[data-parallax="active"]').each(function(index,el){var bg=$(this).data('parallax-src');if(bg!="default"){bg="url("+bg+") center top no-repeat";$(this).css('background',bg);}});


		$(document).scroll(function(event) {
			$('[data-parallax="active"]').each(function(index, el) {
				var x = $(this).data("parallax-scroll");
				var yPos = ($(window).scrollTop() / x); 

				var bgpos = '50% '+ yPos + 'px';

				$(this).css('background-position', bgpos);
			});
		});

		$('[data-parallax="active"]').each(function(index, el) {
			var bg = $(this).data('parallax-src');

			if(bg != "default"){
				bg = "url("+bg+") center top no-repeat";
				$(this).css('background-image', bg);
			}
		});

	//função que centraliza as imagens dentro da classe miniatura
	// o famoso GV8 CENTRALIZER
/*    $('.miniatura img').each(function(){
		var width = $(this).parent('.miniatura').width() / 2;
		var left = parseInt($(this).css('width')) / 2;  
		left = width - left;
		$(this).css('left',  left);

		var height = $(this).parent('.miniatura').height() / 2;
		var top = parseInt($(this).css('height')) / 2;  
		top = height - top;                            
		$(this).css('top', top);

		$(this).parent('.miniatura').addClass('exibir');

	});  */

	//função que centraliza as imagens dentro da classe miniatura
	// o famoso GV8 CENTRALIZER
	$('.mini img').each(function(){
		var width = $(this).parent('.mini').width() / 2;
		var left = parseInt($(this).css('width')) / 2;  
		left = width - left;                            
		$(this).css('left',  left);    

		var height = $(this).parent('.mini').height() / 2;
		var top = parseInt($(this).css('height')) / 2;  
		top = height - top;                            
		$(this).css('top', top);

		$(this).parent('.mini').addClass('exibir');

	});

	// Criado por Marlon
	$(":input[data-msg-validacao]").each(function(){
		$(this).popover({
			content: $(this).data('msg-validacao'),
			placement: 'right',
			trigger: 'focus',
			container: 'body'
		});
	});

});

// Corecao do app de newsletter
$('#wp-email-capture-name-widget').keydown(function(event) {
	$('label[for="wp-email-capture-name-widget"]').addClass('hidden')
});

$('#wp-email-capture-name-widget').blur(function(event) {
	if ($('#wp-email-capture-name-widget').val() == '') {
		$('label[for="wp-email-capture-name-widget"]').removeClass('hidden')
	};
});

$('#wp-email-capture-email-widget').keydown(function(event) {
	$('label[for="wp-email-capture-email-widget"]').addClass('hidden')
});

$('#wp-email-capture-email-widget').blur(function(event) {
	if ($('#wp-email-capture-email-widget').val() == '') {
		$('label[for="wp-email-capture-email-widget"]').removeClass('hidden')
	};
});
/* função para ativar o placeholder no IE 8 e 9 : */
function ativarPlaceholder(seletor, corFundo) {
	/* jQuery necessário */
	var form = jQuery(seletor);
	if (corFundo == null) {
		var corFundo = "#FFFFFF";
	}
	form.find("[placeholder]").each(function() {
		var el = jQuery(this);

		var h = el.outerHeight();
		var tag = el.prop('tagName').toLowerCase();
		
		var html = '';

		if (tag=='textarea') {
			html = '<textarea class="placeholder '+this.className+'" style="border-color:transparent;color:#999999;display:block;height:'+h+'px;margin-bottom:-'+h+'px;overflow:hidden;position:relative;" onfocus="jQuery(this).next().focus();return false;">'+el.attr('placeholder')+'</textarea>';
		} else {
			html = '<input type="text" class="placeholder '+this.className+'" style="border-color:transparent;color:#999999;display:block;height:'+h+'px;margin-bottom:-'+h+'px;overflow:hidden;position:relative;" value="'+el.attr('placeholder')+'" onfocus="jQuery(this).next().focus();return false;" />';
		}

		el.before(html);
		el.css({backgroundColor:'transparent', position: 'relative'});

		setTimeout(function () {
			if (el.val() != "" || (tag == 'textarea' && el.text() != "")) {
				el.css({backgroundColor:corFundo});
			}
		}, 300);
	}).on("focus", function(){
		jQuery(this).css({backgroundColor:corFundo});
	}).on("blur mouseover", function(){
		var el = jQuery(this);
		if (el.val() == "") {
			el.css({backgroundColor:'transparent'});
		} else {
			el.css({backgroundColor:corFundo});
		}
	});
}